#!/usr/bin/env python3
import json
import urllib.request

STEP_1 = 1
STEP_2 = 2
STEP_3 = 3
STEP_4 = 4


def _load():
    link = "https://api.corona-zahlen.org/districts/history/frozen-incidence/30"
    f = urllib.request.urlopen(link)
    out = json.load(f)
    return {k: v for k, v in out["data"].items() if k.startswith("08")}

def _map_incidence(day):
    v = day["weekIncidence"]
    if v >= 50:
        return STEP_4
    if v >= 35:
        return STEP_3
    if v >= 10:
        return STEP_2
    return STEP_1

def _get_incidence(data):
    out = {"name": data["name"]}
    h = data["history"]
    h.reverse()
    history = [_map_incidence(x) for x in h]
    possibilities = {STEP_1, STEP_2, STEP_3, STEP_4}
    for i in range(len(history)-4):
        values = [history[i], history[i+1], history[i+2], history[i+3], history[i+4]]
        for p in set(possibilities):
            if all([x != p for x in values]):
                possibilities.remove(p)
        if len(possibilities) == 1:
            out["step"] = possibilities.pop()
            break
    if "step" not in out:
        raise ValueError()
    return out

def load_incidence():
    data = _load()
    return {x: _get_incidence(y) for x,y in data.items()}
