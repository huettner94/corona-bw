# Corona BW Overview

THIS CODE IS NOW ARCHIVED.

The corona regulations in Baden Württemberg starting from 2021-08-16 are too simple to require this site any longer.

## Datasources
* https://api.corona-zahlen.org/docs/ (https://github.com/marlon360/rki-covid-api)
* https://www.baden-wuerttemberg.de/fileadmin/redaktion/dateien/PDF/Coronainfos/210625_Auf_einen_Blick_DE.pdf

## Used external icons
* https://shields.io/
* https://fontawesome.com/v5.15/icons/virus?style=solid

## License

This code is available under GPL v3 as specified in LICENSE.txt
